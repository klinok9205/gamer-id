<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>
<div id="tooltip" style="position: absolute; display: none;"></div>
<div class="apartments-two-columns">

    <div class="apartments-scheme right">
        <a href="/genplan/params/" class="btn btn-param">Подбор по параметрам</a>

        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 959 1091">
			<? foreach ($arResult["ITEMS"] as $key => $item) {
				if (\COption::GetOptionString("askaron.settings", "UF_SHOW_PRICE")):
					$MIN_PRICE = "<br> от " . round($item["FIRST_ELEMENT"]["PROPERTY_PRICE_VALUE"] / 1000000, 1) . " млн.р.";
					if (!$item["ELEMENTS_COUNT"]) {
						$item["ELEMENTS_COUNT"] = 0;
						$MIN_PRICE = "";
					}
				endif;
				$item["ELEMENTS_COUNT"] = num2word($item["ELEMENTS_COUNT"], array('Квартира', 'Квартиры', 'Квартир')); ?>
                <polygon data-id="<?= $item["ID"]; ?>" data-name="<?= $item["NAME"]; ?>" data-tab='tb-floor-tab-2' class="polygon floor-tab-opener-home floor-tab-2 givemeplan"
                         data-href='#' points="<?= $item["PROPERTY_POINTS_VALUE"]; ?>"
                         onmousemove="showTooltip2(evt, '<span class=\'s1\'><?= $item["NAME"]; ?></span><span class=\'s2\'>В продаже</span><span class=\'s3\'><?= $item["ELEMENTS_COUNT"]; ?><?= $MIN_PRICE; ?></span>');" onmouseout="hideTooltip();"
                ></polygon>
			<? } ?>
        </svg>

        <div class="bg">
            <img src="/local/templates/dt_black/img/build3.jpg" alt="">
        </div>
    </div>

    <div class="apartments-scheme scheme-box left">
		<?= $arResult["PLAN"]; ?>
		<? /*<a href="/genplan/" class="back-link"><img src="/local/templates/dt_black/img/back.svg" alt=""><span>к генплану</span></a>

            <div class="floor-tabs-list">
                <div class="tab tb-floor-tab-2" style="display: block;">
                    <div class="t1">2 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            */ ?>
    </div>

</div>