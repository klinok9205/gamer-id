<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>
<div class="apartments-scheme">
    <a href="/genplan/params/" class="btn btn-param">Подбор по параметрам</a>

    <div id="tooltip" style="position: absolute; display: none;"></div>

    <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1920 808">
		<? foreach ($arResult["ITEMS"] as $key => $item) {
			if (\COption::GetOptionString("askaron.settings", "UF_SHOW_PRICE")):
				$MIN_PRICE = "<br> от " . round($item["FIRST_ELEMENT"]["PROPERTY_PRICE_VALUE"] / 1000000, 1) . " млн.р.";
				if (!$item["ELEMENTS_COUNT"]) {
					$item["ELEMENTS_COUNT"] = 0;
					$MIN_PRICE = "";
				}
			endif;
			$item["ELEMENTS_COUNT"] = num2word($item["ELEMENTS_COUNT"], array('Квартира', 'Квартиры', 'Квартир')); ?>
            <polygon data-tab='tb-floor-tab-<?= $key; ?>' onmousemove="showTooltip2(evt, '<span class=\'s1\'><?= $item["NAME"]; ?></span><span class=\'s2\'>В продаже</span><span class=\'s3\'><?= $item["ELEMENTS_COUNT"]; ?><?= $MIN_PRICE; ?></span>');" onmouseout="hideTooltip();"
                     data-id="<?= $item["ID"]; ?>" data-tip="1" class="floor-tab-opener-home floor-tab-<?= $key; ?> givemeplan" points="<?= $item["PROPERTY_POINTS_VALUE"]; ?>"></polygon>
		<? } ?>
    </svg>

    <div class="bg">
        <img src="/img/biuld1.jpg" alt="">
    </div>
</div>

<div class="apartments-scheme scheme-box">
	<?= $arResult["PLAN"]; ?>
	<? /*<div class="floor-tabs-list">
                <div class="tab tb-floor-tab-1" style="display: block;">

                    <div id="tooltip2" style="position: absolute; display: none;">ewfwfwe</div>

                    <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1878 388"> 
                        <polygon onmousemove="showTooltip(evt, '<span class=\'s1\'>№ 220</span><span class=\'s3\'>2 спальни<br> 110 м2<br> 18 790 000 р.</span>');" onmouseout="hideTooltip();" data-id="1" class="polygon" data-href='#' points="209,61 326,61 326,134 294,134 294,181 261,181 261,192 209,191  "></polygon>
                        <polygon data-id="2" class="polygon grey-hover" points="82,58 204,58 204,266 195,266 195,334 82,334  "></polygon>
                    </svg>

                    <div class="bg">
                        <img src="/img/cheme.png" alt="">
                    </div>

                    1

                </div>
            </div>*/ ?>

</div>