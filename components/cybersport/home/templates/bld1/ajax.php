<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


if (CModule::IncludeModule("iblock")){

if($_REQUEST["action"] == "alphabet"){

	$arOrder = array("NAME" => "ASC");
	$arFilter = array("IBLOCK_ID" => $uchefSettings['BRANDS_IBLOCK']);
	$arSelect = array("ID", "NAME", "CODE", "PREVIEW_PICTURE", "PROPERTY_OSNOVNOY");

	if($_REQUEST["alphabet"]){
		$arFilter["NAME"] = $_REQUEST["alphabet"]."%";
	}

	$arResult["OSNOVNOY"] = "<ul class=\"brands-thumbs\">";

	$res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
	while($item = $res->Fetch()){ 

		if($item["PROPERTY_OSNOVNOY_VALUE"] == 1){

			$item["PREVIEW_PICTURE"] = CFile::ResizeImageGet($item['PREVIEW_PICTURE'], array('width'=>179, 'height'=>74), BX_RESIZE_IMAGE_PROPORTIONAL, true, false, false, 90)["src"];    
			$arResult["OSNOVNOY"] .= "<li><a href=\"/brands/{$item["CODE"]}/\"><img src=\"{$item["PREVIEW_PICTURE"]}\"></a></li>";

		}else{

			$arResult["ITEMS"][substr(trim($item["NAME"]),0,1)][] = $item;

		}

	}

	$arResult["OSNOVNOY"] .= "</ul>";

	$arResult["BRANDS_LIST"] = "";

	foreach ($arResult["ITEMS"] as $key => $item) {

		$arResult["BRANDS_LIST"] .= "<p class=\"brands-mobile-headline\">{$key}</p>";
		$arResult["BRANDS_LIST"] .= "<div class=\"brands-list\">";

		foreach ($item as $k => $i) {
			$arResult["BRANDS_LIST"] .= "<div class=\"brand-name\"><a href=\"/brands/{$i["CODE"]}/\">{$i["NAME"]}</a></div>";
		}

		$arResult["BRANDS_LIST"] .= "</div>";
	}

	$html = "<div class=\"ajax\">{$arResult["OSNOVNOY"]}{$arResult["BRANDS_LIST"]}</div>";

	$tip = $_REQUEST["action"];

	echo(json_encode(array("html"=>$html,"tip" => $tip)));
}


if($_REQUEST["action"] == "cat"){

	$arOrder = array("NAME" => "ASC");
	$arFilter = array("IBLOCK_ID" => $uchefSettings['BRANDS_IBLOCK']);
	$arSelect = array("ID", "NAME", "CODE", "PREVIEW_PICTURE", "PROPERTY_OSNOVNOY");

	if($_REQUEST["category"]){
		$arFilter["PROPERTY_CATEGORY"] = $_REQUEST["category"];
	}
	
	if($_REQUEST["categories"]){
		$arFilter["PROPERTY_CATEGORY"] = explode(",",$_REQUEST["categories"]);
	}

	$arResult["OSNOVNOY"] = "<ul class=\"brands-thumbs\">";

	$res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
	while($item = $res->Fetch()){ 

		if($item["PROPERTY_OSNOVNOY_VALUE"] == 1){

			$item["PREVIEW_PICTURE"] = CFile::ResizeImageGet($item['PREVIEW_PICTURE'], array('width'=>179, 'height'=>74), BX_RESIZE_IMAGE_PROPORTIONAL, true, false, false, 90)["src"];    
			$arResult["OSNOVNOY"] .= "<li><a href=\"/brands/{$item["CODE"]}/\"><img src=\"{$item["PREVIEW_PICTURE"]}\"></a></li>";

		}else{

			$arResult["ITEMS"][substr(trim($item["NAME"]),0,1)][] = $item;

		}

	}

	$arResult["OSNOVNOY"] .= "</ul>";

	$arResult["BRANDS_LIST"] = "";

	$arResult["BRANDS_LIST"] .= "<div class=\"brands-list\">";

	foreach ($arResult["ITEMS"] as $key => $item) {

		foreach ($item as $k => $i) {
			$arResult["BRANDS_LIST"] .= "<div class=\"brand-name\"><a href=\"/brands/{$i["CODE"]}/\">{$i["NAME"]}</a></div>";
		}

	}
	
	$arResult["BRANDS_LIST"] .= "</div>";

	$html = "<div class=\"ajax\">{$arResult["OSNOVNOY"]}{$arResult["BRANDS_LIST"]}</div>";

	$tip = $_REQUEST["action"];

	echo(json_encode(array("html"=>$html,"tip" => $tip)));
}

}
?>