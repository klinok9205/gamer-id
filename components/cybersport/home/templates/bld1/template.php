<? if( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true){
	die();
}
$this->setFrameMode(true);
?>
<div id="tooltip" style="position: absolute; display: none;"></div>
<div class="apartments-two-columns">

        <div class="apartments-scheme right">
            <a href="/genplan/params/" class="btn btn-param">Подбор по параметрам</a>



            <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 959 1091">
              <? foreach ($arResult["ITEMS"] as $key => $item) {
				  if (\COption::GetOptionString("askaron.settings", "UF_SHOW_PRICE")):
                $MIN_PRICE = "<br> от ".round($item["FIRST_ELEMENT"]["PROPERTY_PRICE_VALUE"]/1000000,1)." млн.р.";
                if(!$item["ELEMENTS_COUNT"]){
                    $item["ELEMENTS_COUNT"] = 0;
                    $MIN_PRICE = "";
                }
                endif;
                $item["ELEMENTS_COUNT"] = num2word($item["ELEMENTS_COUNT"], array('Квартира', 'Квартиры', 'Квартир'));
               ?>
                <polygon data-id="<?=$item["ID"];?>" data-name="<?=$item["NAME"];?>" data-tab='tb-floor-tab-2' class="polygon floor-tab-opener-home floor-tab-2 givemeplan"
                         data-href='#' points="<?=$item["PROPERTY_POINTS_VALUE"];?>"
                         onmousemove="showTooltip2(evt, '<span class=\'s1\'><?=$item["NAME"];?></span><span class=\'s2\'>В продаже</span><span class=\'s3\'><?=$item["ELEMENTS_COUNT"];?> <?=$MIN_PRICE;?></span>');" onmouseout="hideTooltip();"
                ></polygon>
              <? } ?>
                <?/*<polygon data-id="1" data-tab='tb-floor-tab-3' class="polygon floor-tab-opener floor-tab-3"
                         data-href='#' points="200,933 456,964 649,924 649,957 456,1003 200,968"></polygon>
                <polygon data-id="2" data-tab='tb-floor-tab-4' class="polygon floor-tab-opener floor-tab-4"
                         data-href='#' points="200,896 456,921 649,889 649,924 456,964 200,933"></polygon>
                <polygon data-id="3" data-tab='tb-floor-tab-5' class="polygon floor-tab-opener floor-tab-5"
                         data-href='#' points="200,860 456,880 649,853 649,889 456,921 200,896"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-6' class="polygon floor-tab-opener floor-tab-6"
                         data-href='#' points="200,823 456,836 649,818 649,853 456,880 200,860"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-7' class="polygon floor-tab-opener floor-tab-7"
                         data-href='#' points="200,788 456,795 649,784 649,818 456,838 200,823"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-8' class="polygon floor-tab-opener floor-tab-8"
                         data-href='#' points="200,750 456,754 649,749 649,784 456,795 200,788"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-9' class="polygon floor-tab-opener floor-tab-9"
                         data-href='#' points="200,715 456,712 649,714 649,750 456,754 200,750"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-10' class="polygon floor-tab-opener floor-tab-10"
                         data-href='#' points="200,678 456,672 649,681 649,714 456,712 200,715"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-11' class="polygon floor-tab-opener floor-tab-11"
                         data-href='#' points="200,640 456,629 649,645 649,681 456,672 200,678"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-12' class="polygon floor-tab-opener floor-tab-12"
                         data-href='#' points="200,607 456,589 649,611 649,645 456,629 200,640"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-13' class="polygon floor-tab-opener floor-tab-13"
                         data-href='#' points="200,568 456,546 649,574 649,611 456,589 200,606"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-14' class="polygon floor-tab-opener floor-tab-14"
                         data-href='#' points="200,531 456,505 649,540 649,574 456,546 200,568"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-15' class="polygon floor-tab-opener floor-tab-15"
                         data-href='#' points="200,495 456,462 649,505 649,540 456,505 200,531"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-16' class="polygon floor-tab-opener floor-tab-16"
                         data-href='#' points="200,459 456,420 649,471 649,505 456,462 200,495"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-17' class="polygon floor-tab-opener floor-tab-17"
                         data-href='#' points="200,421 456,376 649,434 649,471 456,420 200,459"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-18' class="polygon floor-tab-opener floor-tab-18"
                         data-href='#' points="200,383 456,333 649,397 649,435 456,374 200,421"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-19' class="polygon floor-tab-opener floor-tab-19"
                         data-href='#' points="200,346 456,292 649,363 649,397 456,333 200,383"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-20' class="polygon floor-tab-opener floor-tab-20"
                         data-href='#' points="200,312 456,249 649,328 649,363 456,292 200,346"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-21' class="polygon floor-tab-opener floor-tab-21"
                         data-href='#' points="200,274 456,208 649,292 649,328 456,249 200,312"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-22' class="polygon floor-tab-opener floor-tab-22"
                         data-href='#' points="200,233 456,157 649,255 649,292 456,206 200,274"></polygon>
                <polygon data-id="4" data-tab='tb-floor-tab-23-24' class="polygon floor-tab-opener floor-tab-23-24"
                         data-href='#' points="250,96 432,28 606,136 604,204 456,124 250,191"></polygon>*/?>

            </svg>

            <div class="bg">
                <img src="/local/templates/dt_black/img/build2.jpg" alt="">
            </div>
        </div>

        <div class="apartments-scheme scheme-box left">
            <?=$arResult["PLAN"];?>
            <?/*
            <a href="" class="back-link"><img src="/local/templates/dt_black/img/back.svg" alt=""><span>к генплану</span></a>

            <div class="floor-tabs-list">
                <div class="tab tb-floor-tab-2" style="display: block;">
                    <div class="t1">2 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <?/*<div class="tab tb-floor-tab-3">
                    <div class="t1">3 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-4">
                    <div class="t1">4 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-5">
                    <div class="t1">5 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-6">
                    <div class="t1">6 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-7">
                    <div class="t1">7 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-8">
                    <div class="t1">8 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-9">
                    <div class="t1">9 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-10">
                    <div class="t1">10 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-11">
                    <div class="t1">11 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-12">
                    <div class="t1">12 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-13">
                    <div class="t1">13 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-14">
                    <div class="t1">14 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-15">
                    <div class="t1">15 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>

                <div class="tab tb-floor-tab-16">
                    <div class="t1">16 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-17">
                    <div class="t1">17 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-18">
                    <div class="t1">18 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-19">
                    <div class="t1">19 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-20">
                    <div class="t1">20 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-21">
                    <div class="t1">21 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-22">
                    <div class="t1">22 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="tab tb-floor-tab-23-24">
                    <div class="t1">23-24 этаж</div>
                    <div class="t2">В продаже нет квартир</div>
                    <div class="scheme-wrap floor-cheme">
                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                            <polygon data-id="1" class="polygon" data-href='#' points="0,0 387,0 387,117 346,117 346,127 221,127 221,178 163,178 163,276 0,276  "></polygon>
                        </svg>
                        <div class="bg ">
                            <img src="/local/templates/dt_black/img/cheme2.png" alt="">
                        </div>
                    </div>
                </div>*/?>
            </div>

        </div>

    </div>