<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!isset($arParams["CACHE_TIME"])) {
	$arParams["CACHE_TIME"] = 3600000;
}

$arFilter = array("ACTIVE" => "Y");

$arFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];

if ($arParams["SECTION_ID"]) {
	$arFilter["SECTION_ID"] = $arParams["SECTION_ID"];
	$arFilter["INCLUDE_SUBSECTION"] = "Y";
}

if ($arParams["SECTION_CODE"]) {
	$arFilter["SECTION_CODE"] = $arParams["SECTION_CODE"];
	$arFilter["INCLUDE_SUBSECTION"] = "Y";
}

$arFilter = array_merge($arFilter, $arParams["DOP_FILTER"]);
$arSelect = $arParams["FIELD_CODE"];
$arOrder = array("ID" => "ASC");

if ($_REQUEST["SORT_BY"]) {
	$arOrder = array($_REQUEST["SORT_BY"] => $_REQUEST["SORT_ORDER"]);
}

function giveMePlan($arFilter, $ID)
{

	$arFilter["ID"] = $ID;

	$PLAN = "";

	$arSelect[] = "ID";
	$arSelect[] = "NAME";
	$arSelect[] = "PROPERTY_PLAN";

	CModule::IncludeModule('iblock');
	$res = CIBlockElement::GetList($arOrder, $arFilter, false, array("nTopCount" => 1), $arSelect);
	if ($item = $res->Fetch()) {

		if ($item["PROPERTY_PLAN_VALUE"]) {
			$item["PLAN"] = CFile::GetPath($item["PROPERTY_PLAN_VALUE"]);
		}

		$arPoints = array();

		if ($item["ID"]) {
			$arFilter = array("IBLOCK_ID" => 15,
				"PROPERTY_BINDING_TO_FLOOR" => $item["ID"],
				"INCLUDE_SUBSECTIONS" => "Y");

			$points = CIBlockElement::GetList($arOrder, $arFilter, false, array(), array("ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_POINTS_TO_FLOOR", "PROPERTY_STATUS", "PROPERTY_PRICE"));
			while ($point = $points->getNext()) {
				$arPoints[] = $point;
			}
		}
		if ($_REQUEST["TIP"] == 1) {

			if ($_GET["ID"] == 637): $width = 350;
			else: $width = 375; endif;

			$PLAN .= "<div class=\"floor-tabs-list\"><div class=\"tab tb-floor-tab-1\" style=\"display: block;\">";

			$PLAN .= "<div id=\"tooltip2\" style=\"position: absolute; display: none;\">ewfwfwe</div>";

			$PLAN .= "<svg class=\"plan-svg\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" viewBox=\"0 0 1680 " . $width . "\">";
			foreach ($arPoints as $point) {
				$status = "";
				if ($point["PROPERTY_STATUS_VALUE"] && $point["PROPERTY_STATUS_VALUE"] !== "Свободна") {
					$status = " reserved";
				}
				$PLAN .= "    <polygon data-id=\"1\" class=\"polygon" . $status . "\" data-href=\"" . $point["DETAIL_PAGE_URL"] . "\" points=\"" . $point["PROPERTY_POINTS_TO_FLOOR_VALUE"] . "\"></polygon>";
			}

			$PLAN .= "</svg>";

			$PLAN .= "<div class=\"bg\">";
			$PLAN .= "    <img src=\"" . $item["PLAN"] . "\" alt=\"" . $item["NAME"] . "\">";
			$PLAN .= "</div>";
			$PLAN .= "</div>";
			$PLAN .= "</div>";

		} else {

			$PLAN .= "<a href=\"/genplan/\" class=\"back-link\"><img src=\"/local/templates/dt_black/img/back.svg\" alt=\"\"><span>к генплану</span></a>";
			$PLAN .= "<div class=\"floor-tabs-list\">";
			$PLAN .= "    <div class=\"tab tb-floor-tab-2\" style=\"display: block;\">";
			$PLAN .= "        <div class=\"t1\">" . $item["NAME"] . "</div>";
			//  $PLAN .= "        <div class=\"t2\">В продаже нет квартир</div>";
			$PLAN .= "        <div class=\"scheme-wrap floor-cheme\">";
			$PLAN .= "            <svg class=\"plan-svg\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" viewBox=\"0 0 780 650\">";
			foreach ($arPoints as $point) {
				$status = "";
				if ($point["PROPERTY_STATUS_VALUE"] && $point["PROPERTY_STATUS_VALUE"] !== "Свободна") {
					$status = " reserved";
				}
				$PLAN .= "    			<polygon data-id=\"1\" class=\"polygon" . $status . "\" data-href=\"" . $point["DETAIL_PAGE_URL"] . "\" points=\"" . $point["PROPERTY_POINTS_TO_FLOOR_VALUE"] . "\"></polygon>";
			}
			$PLAN .= "            </svg>";
			$PLAN .= "            <div class=\"bg \">";
			$PLAN .= "    				<img src=\"" . $item["PLAN"] . "\" alt=\"" . $item["NAME"] . "\">";
			$PLAN .= "            </div>";
			$PLAN .= "        </div>";
			$PLAN .= "    </div>";
			$PLAN .= "</div>";

		}
	}

	return $PLAN;
}

if ($_REQUEST["ACTION"] == "GIVEMEPLAN" && $_REQUEST["ID"]) {
	$APPLICATION->RestartBuffer();
	echo giveMePlan($arFilter, $_REQUEST["ID"]);
	die();
}

if ($this->StartResultCache(false, array(
	$arFilter,
	$arOrder,
	$arSelect
))
) {

	if (!CModule::IncludeModule("iblock")) {
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arFilterByElements = array("IBLOCK_ID" => 15, "ACTIVE" => "Y");

	$res = CIBlockElement::GetList($arOrder, $arFilter, false, array(), $arSelect);
	while ($item = $res->Fetch()) {
		$arResult["ITEMS"][$item["ID"]] = $item;
		$arFilterByElements["PROPERTY_BINDING_TO_FLOOR"][] = $item["ID"];
	}

	$res = CIBlockElement::GetList(array("PROPERTY_PRICE" => "ASC"), $arFilterByElements, false, array(), array("ID", "NAME", "PROPERTY_PRICE", "PROPERTY_BINDING_TO_FLOOR", "PROPERTY_STATUS"));
	while ($item = $res->Fetch()) {
		$arResult["ITEMS"][$item["PROPERTY_BINDING_TO_FLOOR_VALUE"]]["ELEMENTS"][] = $item;
		// echo"<PRE>"; print_r($item); echo"</PRE>";
		if ($item["PROPERTY_STATUS_VALUE"] == "" or $item["PROPERTY_STATUS_ENUM_ID"] == 3) {
			$arResult["ITEMS"][$item["PROPERTY_BINDING_TO_FLOOR_VALUE"]]["ELEMENTS_COUNT"]++;
		}
		$arResult["ITEMS"][$item["PROPERTY_BINDING_TO_FLOOR_VALUE"]]["ELEMENTS_PRICE"][] = $item["PROPERTY_PRICE_VALUE"];
		if (!$arResult["ITEMS"][$item["PROPERTY_BINDING_TO_FLOOR_VALUE"]]["FIRST_ELEMENT"] && $item["PROPERTY_PRICE_VALUE"] > 0) {
			$arResult["ITEMS"][$item["PROPERTY_BINDING_TO_FLOOR_VALUE"]]["FIRST_ELEMENT"] = $item;
		}
	}

	$FIRST = $arResult["ITEMS"];
	$FIRST = array_shift($FIRST);
	if ($arParams["FLOOR"] > 0) $FIRST["ID"] = $arParams["FLOOR"];
	if ($FIRST["ID"]) {

		$arResult["PLAN"] = giveMePlan($arFilter, $FIRST["ID"]);

	}

	$this->SetResultCacheKeys(array(
		"CATEGORIES",
		"ONSOVNOY",
		"ITEMS",
	));

	$this->IncludeComponentTemplate();
}

?>