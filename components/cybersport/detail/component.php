<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if( ! isset($arParams["CACHE_TIME"])){
	$arParams["CACHE_TIME"] = 600;
}

$arFilter = array("ACTIVE" => "Y");

$arFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
$arFilter["CODE"] = $arParams["ELEMENT_CODE"];

if($arParams["SECTION_ID"]){
	$arFilter["SECTION_ID"] = $arParams["SECTION_ID"];
	$arFilter["INCLUDE_SUBSECTION"] = "Y";
}

if($arParams["SECTION_CODE"]){
	$arFilter["SECTION_CODE"] = $arParams["SECTION_CODE"];
	$arFilter["INCLUDE_SUBSECTION"] = "Y";
}

$arSelect = $arParams["FIELD_CODE"];
$arOrder = array("NAME" => "ASC");

if($_REQUEST["SORT_BY"]){
	$arOrder = array($_REQUEST["SORT_BY"] => $_REQUEST["SORT_ORDER"]);
}

$GLOBALS["HIDE_FORM"] = "";

if($this->StartResultCache(false, array(
	($arParams["CACHE_GROUPS"] === "N" ? false : $USER->GetGroups()),
	$bUSER_HAVE_ACCESS,
	$arFilter,
	$arOrder,
	$arSelect
))
){

	if(!CModule::IncludeModule("iblock")){
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}
	
	$res = CIBlockElement::GetList($arOrder, $arFilter, false, array(), $arSelect);
	if($ob = $res->GetNextElement()){ 
	 	$arResult = $ob->GetFields();  
	 	$arResult["PROPERTIES"] = $ob->GetProperties();
		$GLOBALS["NAMEFORM"] = "Заказ со старницы: ".$arResult["NAME"]." из корпуса ".$arResult["PROPERTIES"]["TOWER"]["VALUE"];
	}else{
		CHTTP::SetStatus("404 Not Found"); 
		@define("ERROR_404","Y");
		   
		$rsSites = CSite::GetByID(SITE_ID);
		$arSite = $rsSites->Fetch();
		include($_SERVER["DOCUMENT_ROOT"]."/404.php");
		return;
	}

	$this->SetResultCacheKeys(array(
		"PROPERTIES",
		"ONSOVNOY",
		"ITEMS",
	));

	$this->IncludeComponentTemplate();
}

?>