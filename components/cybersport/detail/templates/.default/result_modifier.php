<? if( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true){
	die();
}

	foreach ($arResult["PROPERTIES"] as $key => $item) {
		if($key == "PLAN"){
				$arResult["PROPERTIES"][$key]["VALUE"] = CFile::GetPath($item['VALUE']);  
		}
		if($key == "PLAN_FLOOR"){
				$arResult["PROPERTIES"][$key]["VALUE"] = CFile::GetPath($item['VALUE']);
		}
		if($key == "OTDELKA"){
			foreach ($item['VALUE'] as $key2 => $value) {
				$arResult["PROPERTIES"][$key]["VALUE"][$key2] = CFile::ResizeImageGet($value, array('width'=>625, 'height'=>490), BX_RESIZE_IMAGE_PROPORTIONAL, true, false, false, 100)["src"];  
			}	
		}
		if($key == "PDF"){
				$arResult["PROPERTIES"][$key]["VALUE"] = CFile::GetPath($item['VALUE']);
		}
	}
?>