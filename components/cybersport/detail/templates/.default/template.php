<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
$this->setFrameMode(true);
?>

<div class="flat-details-box">
	<? //print_r($arResult["PROPERTIES"]["PLAN"])?>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-5 col-sm-12">
                <div class="flat-details-text">
                    <a href="/genplan/bld<?=$arResult["PROPERTIES"]["TOWER"]["VALUE"];?>/?floor=<?=$arResult["PROPERTIES"]["BINDING_TO_FLOOR"]["VALUE"];?>" class="back-link mb0"><img src="/img/back.svg" alt=""><span>на этаж</span></a><br>
                    <a href="<?=$_SERVER['HTTP_REFERER'];?>" class="back-link"><img src="/img/back.svg" alt=""><span>к подбору</span></a>
                    <div class="num"><?= $arResult["NAME"]; ?></div>
                    <ul class="list">
						<? if ($arResult["PROPERTIES"]["ROOMS"]["VALUE"]) { ?>
                            <li>
                                <div class="t1">Спален</div>
                                <div class="t2"><?= $arResult["PROPERTIES"]["ROOMS"]["VALUE"]; ?></div>
                            </li>
						<? } ?>
						<? if ($arResult["PROPERTIES"]["SQUARE"]["VALUE"]) { ?>
                            <li>
                                <div class="t1">Площадь</div>
                                <div class="t2"><?= number_format($arResult["PROPERTIES"]["SQUARE"]["VALUE"], 0, " ", " "); ?> м²</div>
                            </li>
						<? } ?>
						<? if ($arResult["PROPERTIES"]["FLOOR"]["VALUE"]) { ?>
                            <li>
                                <div class="t1">Этаж</div>
                                <div class="t2"><?= $arResult["PROPERTIES"]["FLOOR"]["VALUE"]; ?></div>
                            </li>
						<? } ?>
						<? if ($arResult["PROPERTIES"]["TOWER"]["VALUE"]) { ?>
                            <li>
                                <div class="t1">Корпус</div>
                                <div class="t2"><?= $arResult["PROPERTIES"]["TOWER"]["VALUE"]; ?></div>
                            </li>
						<? } ?>
						<? if ($arResult["PROPERTIES"]["SECTION"]["VALUE"]) { ?>
                            <li>
                                <div class="t1">Секция</div>
                                <div class="t2"><?= $arResult["PROPERTIES"]["SECTION"]["VALUE"]; ?></div>
                            </li>
						<? } ?>
						<? /*
                            <li>
                                <div class="t1">С объединением</div>
                                <div class="t2">нет</div>
                            </li>
                            */ ?>
                        <?if(\COption::GetOptionString( "askaron.settings", "UF_SHOW_PRICE")):?>
                        <li>
                            <div class="t1">Цена</div>
                            <div class="t2"><?= number_format($arResult["PROPERTIES"]["PRICE"]["VALUE"], 0, " ", " "); ?> р.</div>
                        </li>
                        <?endif;?>
                    </ul>
                    <style>
                        .flat-details-text .btn {
                            margin-right: 10px !important;
                        }
                    </style>
                    <a href="javascript:void(0);" class="btn lazyscroll">Отправить заявку</a>
                                            <?if($arResult["PROPERTIES"]["PDF"]["VALUE"]):?><a href="<?=$arResult["PROPERTIES"]["PDF"]["VALUE"];?>" class="btn lazyscroll" target="_blank">Скачать презентацию</a><?endif;?>
                </div>
            </div>
            <div class="col-md-6 col-lg-offset-1 col-sm-12">
                <div class="flat-tabs">
                    <div class="flat-tab-list">
						<? if ($arResult["PROPERTIES"]["PLAN"]["VALUE"]) { ?>
                            <div class="tb1-opener bx act">Планировка</div>
						<? } ?>
						<? if ($arResult["PROPERTIES"]["POINTS"]["VALUE"]) { ?>
                            <!--                            <div class="tb2-opener bx">На этаже</div>-->
						<? } ?>
						<? if ($arResult["PROPERTIES"]["OTDELKA"]["VALUE"]) { ?>
                            <div class="tb3-opener bx">Отделка</div>
						<? } ?>
                    </div>
                    <div class="tabs">
						<? if ($arResult["PROPERTIES"]["PLAN"]["VALUE"]) { ?>
                            <div class="tab tab1">
                                <div class="apartments-scheme">
                                    <div class="scheme-wrap floor-cheme">
                                        <div class="bg">
                                            <img src="<?= $arResult["PROPERTIES"]["PLAN"]["VALUE"]; ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
						<? } ?>
						<? if ($arResult["PROPERTIES"]["POINTS"]["VALUE"]) { ?>
                            <div class="tab tab2">
                                <div class="apartments-scheme">
                                    <div class="scheme-wrap floor-cheme">
                                        <svg class="plan-svg" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 780 559">
                                            <polygon data-id="1" class="polygon" data-href='#' points="<?= $arResult["PROPERTIES"]["POINTS"]["VALUE"]; ?>"></polygon>
                                        </svg>
                                        <div class="bg ">
                                            <img src="<?= $arResult["PROPERTIES"]["PLAN_FLOOR"]["VALUE"]; ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
						<? } ?>
						<? if ($arResult["PROPERTIES"]["OTDELKA"]["VALUE"]) { ?>
                            <div class="tab tab3">
                                <div class="owl-carousel flat-image-slider with-arrows">
									<? foreach ($arResult["PROPERTIES"]["OTDELKA"]["VALUE"] as $key => $photo) { ?>
                                        <div class="item"><img src="<?= $photo; ?>" alt=""></div>
									<? } ?>
                                </div>
                            </div>
						<? } ?>
                    </div>
                </div>
                <div class="rose">
                    <img src="/img/compass.svg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
    