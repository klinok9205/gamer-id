<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader,
    Bitrix\Main\SystemException;

class CyberAuth extends CBitrixComponent{

    // public $template = [
    //     'auth' => 'auth.php',
    // ];

    /**
    * Получение списка заметок
    **/
    protected function Ajax($request){
        if($request['action']){
            global $USER;
            $USER = new CUser;

            $errors = [];
            $error_html = '';

            switch ($request['action']) {
                case "auth":
                    $action = 'auth';
                    if(!$request['login']){
                        $errors_key[] = 'login';
                        $errors['login'] = 'Не указан login!';
                        $error_html .= '<p>Не указан login!</p>';
                    }
                    if(!$request['password']){
                        $errors_key[] = 'password';
                        $errors['password'] = 'Не указан пароль!';    
                        $error_html .= '<p>Не указан пароль!</p>';                    
                    }

                    if($arAuthResult = $USER->Login("admin", "123456", "Y")){
                        // echo"<PRE>"; print_r($arAuthResult); echo"</PRE>";
                        if($arAuthResult['TYPE'] == 'ERROR'){
                            $errors['error'] = $arAuthResult['MESSAGE'];   
                            $error_html .= '<p>'.$arAuthResult['MESSAGE'].'</p>';       
                        }else{
                            $result = 'Успешный вход';
                        }
                    }

                    break;
                case "forgot_password":

                        $action = 'forgot_password';

                        if(!$request['email']){
                            $errors['error'] = 'Укажите свой E-mail!';   
                            $error_html .= '<p>Укажите свой E-mail!</p>';
                            break;
                        }

                        $filter = [
                            "EMAIL" => $request['email'],
                        ];

                        $rsUsers = \CUser::GetList($by="id", $order="asc", $filter); // выбираем пользователей
                        if($arUser = $rsUsers->fetch()) {
                            $arEventFields = $arUser;
                            $arEventFields = array_merge($arEventFields,$_SERVER);

                            if(\CEvent::Send("USER_PASS_REQUEST", "s1", $arEventFields)){
                                $result = ['result' => true];
                            }
                        }else{
                            $errors['error'] = 'Такого пользователя не существует!';   
                            $error_html .= '<p>Такого пользователя не существует!</p>';
                        }

                    break;
                case "register":

                    $action = 'register';

                    if(!$request['login']){
                        $errors_key[] = 'login';
                        $errors['login'] = 'Не указан login!';
                        $error_html .= '<p>Не указан login!</p>';
                    }
                    if(!$request['email']){
                        $errors_key[] = 'email';
                        $errors['email'] = 'Не указан email!';    
                        $error_html .= '<p>Не указан email!</p>';                    
                    }
                    if(!$request['password']){
                        $errors_key[] = 'password';
                        $errors['password'] = 'Укажите пароль!';    
                        $error_html .= '<p>Укажите пароль!</p>';                    
                    }
                    if(!$request['password_confirm']){
                        $errors_key[] = 'password_confirm';
                        $errors['password_confirm'] = 'Подтвердите пароль!';    
                        $error_html .= '<p>Подтвердите пароль!</p>';                    
                    }
                    if(!$request['checked_pravila']){
                        $errors_key[] = 'checked_pravila';
                        $errors['checked_pravila'] = 'Подтвердите своё согласие с правилами платформы!';    
                        $error_html .= '<p>Подтвердите своё согласие с правилами платформы!</p>';                    
                    }
                    if(!$request['checked_politica']){
                        $errors_key[] = 'checked_politica';
                        $errors['checked_politica'] = 'Подтвердите своё согласие с политикой обработки персональных данных!';    
                        $error_html .= '<p>Подтвердите своё согласие с политикой обработки персональных данных!</p>';                    
                    }

                    if($arResult = $USER->Register(
                        $request['login'], 
                        $request['login'], 
                        "",
                        $request['password'], 
                        $request['password_confirm'], 
                        $request['email'])){
                        if($arResult['TYPE'] == 'ERROR'){
                            $errors['error'] = $arResult['MESSAGE'];   
                            $error_html .= '<p>'.$arResult['MESSAGE'].'</p>';       
                        }else{
                            // Регистрация прошла успешно!
                            $result = $arResult;
                            // echo $USER->GetID(); // ID нового пользователя
                        }
                    }
                    break;
            }

            if($errors){
                echo json_encode(['result' => false, 'errors' => $errors, 'keys' => $errors_key, 'error_html' => $error_html, 'action' => $action]);
            }else{
                echo json_encode(['result' => true, 'message' => $result, 'action' => $action]);
            }
        }
    }
    /*  
    protected function MyPagination(){
            $total_page = ($GLOBALS["arNavParam"]["PAGE_COUNT"])?$GLOBALS["arNavParam"]["PAGE_COUNT"]:ceil($this->arResult['ITEMS']["LAST_ID"] / PAGE);
    } 
    */

    /**
     * прерывает кеширование
     */
    protected function abortDataCache()
    {
        $this->AbortResultCache();
    }

    public function executeComponent()
    {
        global $APPLICATION;
        try {
            if ($this->StartResultCache()) {
                // echo "<PRE>"; print_r($_REQUEST); echo "</PRE>";
                $request = $_REQUEST;
                if($request && $request['ajax']){
                    $GLOBALS['APPLICATION']->RestartBuffer();
                    $this->Ajax($request);
                    die();
                }
                // $this->MyPagination();
                // if (count($this->arResult['ITEMS']) == 0) {
                //     $this->abortDataCache();
                //     return false;
                // }
                $this->includeComponentTemplate();
            }
        } catch (Exception $e) {
            $this->abortDataCache();
            ShowError($e->getMessage());
        }
    }

}

?>