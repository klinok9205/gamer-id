<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(!$_REQUEST["action"] or $_REQUEST['action'] == 'auth'){?>   
    <div class="form-page__form scroll">
      <div class="form-page__scroll">
        <div class="form-page__content">
          <div class="form-page__form-title">Привет!</div>
          <div class="form-page__form-title-small">Тебя-то мы и ждали!</div>
          <form action="javascript:void(0);">
            <input type="hidden" name="action" value="auth">
            <input type="hidden" name="ajax" value="true">
            <div class="form-group">
              <label for="">Логин</label>
              <input type="text" name="login" class="form-control" placeholder="">
            </div>
            <div class="form-group">
              <label for="">Пароль</label>
              <div class="password">
                <input class='validate' type='password' name='password' id='password' />
                <span toggle="#password" class="field-icon toggle-password">
                  <span class="material-icons">
                    <svg class="icon">
                      <use xlink:href="#eye-icon"></use>
                    </svg>
                  </span>
                </span>
              </div>
            </div>


            <button class="btn btn--xl btn--full">Войти</button>

            <div class="form-page__bottom">
              <div class="checkbox">
                <input type="checkbox" id="checkbox" name="checked_zapomni" checked>
                <label for="checkbox">Запомнить меня </label>
              </div>

              <a href="/?action=forgot_password" class="forgot-password">Я не помню пароль</a>
            </div>


          </form>

          <div class="form-page__social">

              <div class="btn-create">
                <a href="/?action=register" class="btn btn--xl btn--full btn--empty">создать аккаунт</a>
              </div>

            <div class="form-page__social-title">Или используйте вход через:</div>
            <div class="profile-accounts__content center">
              <?
              $APPLICATION->IncludeComponent("cybersport:auth", "main", 
                 array(
                    "AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
                    "SUFFIX"=>"form",
                 ), 
                 $component, 
                 array("HIDE_ICONS"=>"Y")
              );
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
<?}?>
<?if($_REQUEST['action'] == 'forgot_password'){?>
    <div class="form-page__form">
      <div class="form-page__scroll">
        <div class="form-page__content">
          <div class="form-page__form-title">Пароль куда-то
            закатился...</div>
          <div class="form-page__form-title-sm">Всякое бывает. Укажи почту, на которую зарегистрирован аккаунт, и мы отправим туда письмо с инструкцией.</div>
          <form action="javascript:void(0);">
            <input type="hidden" name="action" value="forgot_password">
            <input type="hidden" name="ajax" value="true">
            <div class="form-group">
              <label for="">E-mail</label>
              <input type="text" class="form-control" name="email" value="" placeholder="wasted@cybermos.ru">
            </div>
          


            <button type="submit" class="btn btn--xl btn--full">восстановить пароль</button>

            <div class="form-page__bottom">
            <a href="/" class="back"><span class="icon"><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/back-arrow.svg" alt=""></span>назад к логину</a>
            </div>


          </form>

        
        </div>
      </div>


    </div>
<?}?>
<?if($_REQUEST['action'] == 'register'){?>

    <div class="form-page__form scroll">
      <div class="form-page__scroll" >
        <div class="form-page__content">
          <div class="form-page__form-title">Зарегистрироваться!</div>
  
          <form action="javascript:void(0);">
            <input type="hidden" name="action" value="register">
            <input type="hidden" name="ajax" value="true">
            <div class="form-group">
              <label for="">Никнейм</label>
              <input type="text" name="login" class="form-control" placeholder="">
            </div>
            <div class="form-group">
              <label for="">E-mail</label>
              <input type="email" name="email" class="form-control" placeholder="">
            </div>
            <div class="form-group">
              <label for="">Пароль</label>
              <div class="password">
                <input class='validate' type='password' name='password' id='password' />
                <span toggle="#password" class="field-icon toggle-password">
                  <span class="material-icons">
                    <svg class="icon">
                      <use xlink:href="#eye-icon"></use>
                    </svg>
                  </span>
                </span>
              </div>
              </div>
             
            <div class="form-group">
              <label for="">Повторить пароль</label>
              <div class="password">
                <input class='validate' type='password' name='password_confirm' name='confirm-password' id='confirm-password' />
                <span toggle="#confirm-password" class="field-icon toggle-password">
                  <span class="material-icons">
                    <svg class="icon">
                      <use xlink:href="#eye-icon"></use>
                    </svg>
                  </span>
                </span>
              </div>
            
            </div>
            
            <button class="btn btn--xl btn--full">создать аккаунт</button>
  
            <div class="checkbox">
              <input type="checkbox" id="checkbox" name="checked_pravila" checked>
              <label for="checkbox">Я принимаю правила платформы </label>
            </div>
  
            <div class="checkbox">
              <input type="checkbox" id="checkbox2" name="checked_politica" checked>
              <label for="checkbox2">Я соглашаюсь с политикой обработки персональных данных </label>
            </div>
          </form>
  
          <div class="form-page__social">
            <div class="form-page__social-title">Или используйте вход через:</div>
            <div class="profile-accounts__content center">
              <span class="acccount-item"><a href=""><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/vk-icon.svg" alt=""></a></span>
              <span class="acccount-item"><a href=""><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/steam-icon.svg" alt=""></a></span>
              <span class="acccount-item"><a href=""><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/discord-icon.svg" alt=""></a></span>
              <span class="acccount-item"><a href=""><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/twich-icon.svg" alt=""></a></span>
              <span class="acccount-item"><a href=""><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/google-icon.svg" alt=""></a></span>
            </div>
          </div>
        </div>
      </div>
    </div>
<?}?>