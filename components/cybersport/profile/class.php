<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader,
    Bitrix\Main\SystemException;

class CyberProfile extends CBitrixComponent{

    /**
    * Профиль
    **/
    protected function Profile(){
            global $USER;
            $rsUser = CUser::GetByID($USER->GetID());
            $arUser = $rsUser->Fetch();
            $this->arResult = $arUser;
    }
    /*  
    protected function MyPagination(){
            $total_page = ($GLOBALS["arNavParam"]["PAGE_COUNT"])?$GLOBALS["arNavParam"]["PAGE_COUNT"]:ceil($this->arResult['ITEMS']["LAST_ID"] / PAGE);
    } 
    */

    /**
     * прерывает кеширование
     */
    protected function abortDataCache()
    {
        $this->AbortResultCache();
    }

    public function executeComponent()
    {
        global $APPLICATION;
        try {
            if ($this->StartResultCache()) {
                $this->Profile();
                $this->includeComponentTemplate();
            }
        } catch (Exception $e) {
            $this->abortDataCache();
            ShowError($e->getMessage());
        }
    }

}

?>