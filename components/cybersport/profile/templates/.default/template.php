<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

   <div class="container">
      <div class="main-content">
        <aside>
          <div class="side-menu move-1">
            <a href="" class="menu-item active">
              <span class="menu-item__icon">
                <svg class="icon" width="19px" height="22px">
                  <use xlink:href="#menu-icon-1"></use>
                </svg>
              </span>
              <span class="menu-item__title">Обзор учетной записи</span>
            </a>

            <a href="" class="menu-item">
              <span class="menu-item__icon">
                <svg class="icon" width="16px" height="22px">
                  <use xlink:href="#menu-icon-2"></use>
                </svg>
              </span>
              <span class="menu-item__title">личные данные</span>
            </a>

            <a href="" class="menu-item">
              <span class="menu-item__icon">
                <svg class="icon" width="22px" height="25px">
                  <use xlink:href="#menu-icon-3"></use>
                </svg>
              </span>
              <span class="menu-item__title">Статистика</span>
            </a>

            <a href="" class="menu-item">
              <span class="menu-item__icon">
                <svg class="icon" width="26px" height="10px">
                  <use xlink:href="#menu-icon-4"></use>
                </svg>
              </span>
              <span class="menu-item__title">Связанные сервисы </span>
            </a>

            <a href="" class="menu-item">
              <span class="menu-item__icon">
                <svg class="icon" width="20px" height="23px">
                  <use xlink:href="#menu-icon-5"></use>
                </svg>
              </span>
              <span class="menu-item__title">Безопасность
              </span>
            </a>

            <a href="" class="menu-item">
              <span class="menu-item__icon">
                <svg class="icon" width="24px" height="24px">
                  <use xlink:href="#menu-icon-5"></use>
                </svg>
              </span>
              <span class="menu-item__title">оплата</span>
            </a>
          </div>
        </aside>
        <div class="content">
          <h1 class="h1">Обзор учетной записи</h1>

          <div class="profile-main">
            <div class="profile-main__left">
              <div class="profile-main__avatar">
                <img src="<?php echo SITE_TEMPLATE_PATH ?>/images/user-empty.png" class="profile-main__avatar-image" alt="">
              </div>
              <a href="#" class="profile-main__social">
                <svg class="icon">
                  <use xlink:href="#vk"></use>
                </svg>
              </a>
            </div>


            <div class="profile-main__content">
              <span class="profile-main__name"><?=$arResult['NAME'];?> <?=$arResult['LAST_NAME'];?></span>
              <span class="profile-main__nickname"><?=$arResult['LOGIN'];?></span>

              <div class="profile-main__bottom">
                <div class="profile-main__btns">
                  <a href="#" class="btn btn--fixed">Активировать</a>
                  <a href="#" class="btn btn--fixed disabled">Верифицировать</a>
                </div>
                <div class="profile-main__dates">
                  <?/*<PRE><?print_r($arResult);?></PRE>*/?>
                  <div class="profile-main__date-item">аккаунт создан:<span> <?=substr($arResult['DATE_REGISTER'],0,10);?></span></div>
                  <div class="profile-main__date-item">последняя авторизация: <span><?=substr($arResult['LAST_LOGIN'],0,10);?></span></div>
                </div>
              </div>
            </div>
          </div>
      <?
          $arResult["AUTH_SERVICES"] = false;
          if(CModule::IncludeModule("socialservices")) {
            $oAuthManager = new CSocServAuthManager();
            $arServices = $oAuthManager->GetActiveAuthServices($arResult);
            if(!empty($arServices)) $arResult["AUTH_SERVICES"] = $arServices;
          }

      $APPLICATION->IncludeComponent("cybersport:auth", "flat", 
         array(
            "AUTH_SERVICES"=>$arResult["AUTH_SERVICES"],
            "SUFFIX"=>"form",
         ), 
         $component, 
         array("HIDE_ICONS"=>"Y")
      );
      ?> 

          <h2 class="h2">Твое участие в игровых системах</h2>

          <div class="game-systems">
            <div class="game-system active">
              <div class="game-system__content">
                <span class="game-system__logotype"><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/logo-mk.png" alt=""></span>
                <span class="game-system__title">Московский Киберспорт</span>

                <div class="game-system__info">
                  <p>Проводим мероприятия и турниры по различным киберспортивным дисциплинам. Организовываем системы
                    подготовки спортсменов.</p>
    
                  <div class="game-system__stats">
                    <div class="game-system__stat-item">
                      <span class="icon"><img src="images/battle-icon.png" alt=""></span>
                      <span class="game-system__stat-item-number">32</span>
                      <span class="game-system__stat-item-title">Турниров</span>
                    </div>
                    <div class="game-system__stat-item">
                      <span class="icon"><img src="images/battle-icon.png" alt=""></span>
                      <span class="game-system__stat-item-number">47</span>
                      <span class="game-system__stat-item-title">Матчей</span>
                    </div>
                  </div>
    
                  <div class="game-system__date">дата авторизации:<span> 17.01.2019</span></div>
    
                  <div class="game-system__status game-system__status--active">
                    <span class="icon"><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/active-icon.svg" alt=""></span>
                    Аккаунт активен!
                  </div>
                  <div class="game-system__status game-system__status--banned">
                    <span class="icon"><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/banned-icon.svg" alt=""></span>
                    Аккаунт забанен до 27.02.2023
                  </div>
                  <a href="" class="game-system__status game-system__status--join">
                    <span class="icon">+</span>
                    Присоединиться!
                  </a>
                </div>
               
                <div class="game-system__btn">
                  <a href="" class="btn btn--fixed">подробнее</a>
                </div>
  
              </div>
          
            </div>
            <div class="game-system banned">
              <div class="game-system__content">
                <span class="game-system__logotype"><img src="images/logo-mskl.png" alt=""></span>
                <span class="game-system__title">Московская студенческая
                  киберспортивная лига</span>
              
                  <div class="game-system__info">
                    <p>Проводим мероприятия и турниры по различным киберспортивным дисциплинам. Организовываем системы
                      подготовки спортсменов.</p>
      
                    <div class="game-system__stats">
                      <div class="game-system__stat-item">
                        <span class="icon"><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/battle-icon.png" alt=""></span>
                        <span class="game-system__stat-item-number">32</span>
                        <span class="game-system__stat-item-title">Турниров</span>
                      </div>
                      <div class="game-system__stat-item">
                        <span class="icon"><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/battle-icon.png" alt=""></span>
                        <span class="game-system__stat-item-number">47</span>
                        <span class="game-system__stat-item-title">Матчей</span>
                      </div>
                    </div>
      
                    <div class="game-system__date">дата авторизации:<span> 17.01.2019</span></div>
      
                    <div class="game-system__status game-system__status--active">
                      <span class="icon"><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/active-icon.svg" alt=""></span>
                      Аккаунт активен!
                    </div>
                    <div class="game-system__status game-system__status--banned">
                      <span class="icon"><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/banned-icon.svg" alt=""></span>
                      Аккаунт забанен до 27.02.2023
                    </div>
                    <a href="" class="game-system__status game-system__status--join">
                      <span class="icon">+</span>
                      Присоединиться!
                    </a>
                  </div>
                <div class="game-system__btn">
                  <a href="" class="btn btn--fixed">подробнее</a>
                </div>
  
              </div>
          
            </div>
            <div class="game-system join">
              <div class="game-system__content">
                <span class="game-system__logotype"><img src="images/logo-fskm.png" alt=""></span>
                <span class="game-system__title">Федерация 
                  компьютерного спорта Москвы</span>
            
                  <div class="game-system__info">
                    <p>Проводим мероприятия и турниры по различным киберспортивным дисциплинам. Организовываем системы
                      подготовки спортсменов.</p>
      
                    <div class="game-system__stats">
                      <div class="game-system__stat-item">
                        <span class="icon"><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/battle-icon.png" alt=""></span>
                        <span class="game-system__stat-item-number">32</span>
                        <span class="game-system__stat-item-title">Турниров</span>
                      </div>
                      <div class="game-system__stat-item">
                        <span class="icon"><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/battle-icon.png" alt=""></span>
                        <span class="game-system__stat-item-number">47</span>
                        <span class="game-system__stat-item-title">Матчей</span>
                      </div>
                    </div>
      
                    <div class="game-system__date">дата авторизации:<span> 17.01.2019</span></div>
      
                    <div class="game-system__status game-system__status--active">
                      <span class="icon"><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/active-icon.svg" alt=""></span>
                      Аккаунт активен!
                    </div>
                    <div class="game-system__status game-system__status--banned">
                      <span class="icon"><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/banned-icon.svg" alt=""></span>
                      Аккаунт забанен до 27.02.2023
                    </div>
                    <a href="" class="game-system__status game-system__status--join">
                      <span class="icon">+</span>
                      Присоединиться!
                    </a>
                  </div>
                <div class="game-system__btn">
                  <a href="" class="btn btn--fixed">подробнее</a>
                </div>
  
              </div>
          
            </div>
          </div>


        </div>
      </div>
    </div>