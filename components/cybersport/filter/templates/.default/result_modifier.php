<? if( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true){
	die();
}

	if($arResult["OSNOVNOY"]){

		foreach ($arResult["OSNOVNOY"] as $key => $brend) {
			
			if($brend["PREVIEW_PICTURE"]){

				$arResult["ALFAVIT_KEYS"][] = substr($brend["NAME"], 0, 1);

				$arResult["OSNOVNOY"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet($brend['PREVIEW_PICTURE'], array('width'=>179, 'height'=>74), BX_RESIZE_IMAGE_PROPORTIONAL, true, false, false, 90)["src"];    

			}

		}

	}

	if($arResult["CATEGORIES"]){

		foreach ($arResult["CATEGORIES"] as $key => $category) {
			
			if($category["PICTURE"]){

				$arResult["CATEGORIES"][$key]["PICTURE"] = CFile::ResizeImageGet($category['PICTURE'], array('width'=>179, 'height'=>74), BX_RESIZE_IMAGE_PROPORTIONAL, true, false, false, 90)["src"];    

			}

		}

	}

	if($arResult["ITEMS"]){

		$arResult["ALFAVIT_KEYS"] = array_merge($arResult["ALFAVIT_KEYS"],array_keys($arResult["ITEMS"]));

	}

	$arResult["ALFAVIT_NUMBER"] = range(0, 9);
	$arResult["ALFAVIT_EN"] = range('A', 'Z');
	$arResult["ALFAVIT_RU"] = array();
	foreach (range(chr(0xC0),chr(0xDF)) as $v)
	$arResult["ALFAVIT_RU"][] = iconv('CP1251','UTF-8',$v);

	// echo"<PRE>"; print_r($arResult); echo"</PRE>";
?>