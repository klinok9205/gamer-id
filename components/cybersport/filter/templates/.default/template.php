<? if( ! defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true){
	die();
}
$this->setFrameMode(true);
if($_REQUEST['test']){
    echo "<PRE>"; print_r($_REQUEST); echo "</PRE>";
}
?><form id="flat_form" action="javascript:void(0);">
    <input name="ACTION" type="hidden" value="AJAX">
                    <div class="filt-bx">
                        <?if($arResult["TOWER"]){?>
                        <div class="bx">
                            <div class="t">Корпус</div>
                            <ul class="num-list">
                                <? foreach ($arResult["TOWER"] as $key => $item) { ?>
                                <li data-tower="<?=$item?>">
                                    <label>
                                        <input type="checkbox" name="PROPERTY_TOWER[]"<?if($_REQUEST["PROPERTY_TOWER"] && in_array($item, $_REQUEST["PROPERTY_TOWER"])){?> checked<?}?> value="<?=$item;?>">
                                        <span><?=$item?></span>
                                    </label>
                                </li>
                                <? } ?>
                            </ul>
                        </div>
                        <?}?>
                        <?if($arResult["ROOMS"]){?>
                        <div class="bx">
                            <div class="t">Комнат</div>
                            <ul class="num-list">
                                <? foreach ($arResult["ROOMS"] as $key => $item) { ?>
                                <li data-rooms="<?=$item?>">
                                    <label>
                                        <input type="checkbox" name="PROPERTY_ROOMS[]"<?if($_REQUEST["PROPERTY_ROOMS"] && in_array($item, $_REQUEST["PROPERTY_ROOMS"])){?> checked<?}?> value="<?=$item;?>">
                                        <span><?=$item?></span>
                                    </label>
                                </li>
                                <? } ?>
                            </ul>
                        </div>
                        <?}?>
                        <?if($arResult["SQUARE"]){
                            if($_REQUEST['PROPERTY_SQUARE']){
                                $ex = explode(";",$_REQUEST['PROPERTY_SQUARE']);
                                $arResult["SQUARE"]["FROM"] = $ex[0];
                                $arResult["SQUARE"]["TO"] = $ex[1];
                            }?>
                        <div class="bx bx-sl" data-square="true">
                            <div class="t">Площадь (м<sup>2</sup>)</div>
                            <div class="pr-slider">
                                <input type="text" class="price-slider" name="PROPERTY_SQUARE" data-min="<?=$arResult["SQUARE"]["MIN"];?>" data-max="<?=$arResult["SQUARE"]["MAX"];?>" value=""
                                <?if($arResult["SQUARE"]["FROM"]){?>
                                data-from="<?=$arResult["SQUARE"]["FROM"];?>"
                                <?}?>
                                <?if($arResult["SQUARE"]["TO"]){?>
                                data-to="<?=$arResult["SQUARE"]["TO"];?>"
                                <?}?>/>
                            </div>
                        </div>
                        <?}?>
                        <?if($arResult["PRICE"]){
                            if($_REQUEST['PROPERTY_PRICE']){
                                $ex = explode(";",$_REQUEST['PROPERTY_PRICE']);
                                $arResult["PRICE"]["FROM"] = $ex[0];
                                $arResult["PRICE"]["TO"] = $ex[1];
                            }
                            ?>
                        <div class="bx bx-sl" data-price="true">
                            <div class="t">Cтоимость (млн.р.)</div>
                            <div class="pr-slider">
                                <input type="text" class="price-slider" name="PROPERTY_PRICE" value="<?=$_REQUEST['PROPERTY_PRICE'];?>" data-min="<?=$arResult["PRICE"]["MIN"];?>" data-max="<?=$arResult["PRICE"]["MAX"];?>" value=""
                                <?if($arResult["PRICE"]["FROM"]){?>
                                data-from="<?=$arResult["PRICE"]["FROM"];?>"
                                <?}?>
                                <?if($arResult["PRICE"]["TO"]){?>
                                data-to="<?=$arResult["PRICE"]["TO"];?>"
                                <?}?>
                                />
                            </div>
                        </div>
                        <?}?>
                        <?if($arResult["FLOOR"]){
                            if($_REQUEST['PROPERTY_FLOOR']){
                                $ex = explode(";",$_REQUEST['PROPERTY_FLOOR']);
                                $arResult["FLOOR"]["FROM"] = $ex[0];
                                $arResult["FLOOR"]["TO"] = $ex[1];
                            }?>
                        <div class="bx bx-sl" data-floor="true">
                            <div class="t">Этаж</div>
                            <div class="pr-slider">
                                <input type="text" class="price-slider" name="PROPERTY_FLOOR" data-min="<?=$arResult["FLOOR"]["MIN"];?>" data-max="<?=$arResult["FLOOR"]["MAX"];?>" value=""
                                <?if($arResult["FLOOR"]["FROM"]){?>
                                data-from="<?=$arResult["FLOOR"]["FROM"];?>"
                                <?}?>
                                <?if($arResult["FLOOR"]["TO"]){?>
                                data-to="<?=$arResult["FLOOR"]["TO"];?>"
                                <?}?>/>
                            </div>
                        </div>
                        <?}?>
                    </div>
                    <div class="param-bx">
                        <div class="param-opener"><span>Дополнительные параметры</span><img src="/local/templates/dt_black/img/arr-param.svg" alt=""></div>
                        <div class="param-hidden">
                            <?if($arResult["OSOBENNOST"]){?>
                            <div class="bx">
                                <div class="t">Особенность</div>
                                <ul class="num-list param-list">
                                    <? foreach ($arResult["OSOBENNOST"] as $key => $item) { ?>
                                    <li>
                                        <label>
                                            <input type="checkbox" name="PROPERTY_OSOBENNOST[]" value="<?=$item;?>"<?if(in_array($item, $_REQUEST["PROPERTY_OSOBENNOST"])){?> checked<?}?>>
                                            <span><?=$item;?></span>
                                        </label>
                                    </li>
                                    <? } ?>
                                </ul>
                            </div>
                            <?}?>
                            <?if($arResult["VID"]){?>
                            <div class="bx">
                                <div class="t">Вид</div>
                                <ul class="num-list param-list">
                                    <? foreach ($arResult["VID"] as $key => $item) { ?>
                                    <li>
                                        <label>
                                            <input type="checkbox" name="PROPERTY_VID[]" value="<?=$item;?>"<?if(in_array($item, $_REQUEST["PROPERTY_VID"])){?> checked<?}?>>
                                            <span><?=$item;?></span>
                                        </label>
                                    </li>
                                    <? } ?>
                                </ul>
                            </div>
                            <?}?>
                        </div>

                        <div class="clear-filter"><a href="/genplan/params/" style="font-weight: 500">Сбросить фильтр</a></div>
                    </div>
                </form>

