<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if( ! isset($arParams["CACHE_TIME"])){
	$arParams["CACHE_TIME"] = 3600000;
}

if(!CModule::IncludeModule("iblock")){
	$this->AbortResultCache();
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return;
}

$arFilter = array("ACTIVE" => "Y");

$arFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];

if($arParams["SECTION_ID"]){
	$arFilter["SECTION_ID"] = $arParams["SECTION_ID"];
	$arFilter["INCLUDE_SUBSECTION"] = "Y";
}

if($arParams["SECTION_CODE"]){
	$arFilter["SECTION_CODE"] = $arParams["SECTION_CODE"];
	$arFilter["INCLUDE_SUBSECTION"] = "Y";
}

$arFilter = array_merge($arFilter,$arParams["DOP_FILTER"]);
$arSelect = $arParams["FIELD_CODE"];
$arOrder = array("ID" => "ASC");

if($_REQUEST["SORT_BY"]){
	$arOrder = array($_REQUEST["SORT_BY"] => $_REQUEST["SORT_ORDER"]);
}

function getUniqueValues($PROP,$FILTER=false){

	$ITEMS = array();
	$arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE"=>"Y", "!".$PROP => false);
	if($FILTER){
		unset($FILTER[">=".$PROP]);
		unset($FILTER["<=".$PROP]);
		unset($FILTER["PROPERTY_".$PROP."_VALUE"]);
		$arFilter = array_merge($arFilter,$FILTER);
    }
	$db_list = CIBlockElement::GetList(Array($PROP=>"ASC"), $arFilter, Array($PROP));
	while($ob = $db_list->GetNext())
	{	
		if($ob[$PROP."_VALUE"]){
			$ITEMS[] = $ob[$PROP."_VALUE"];
		}
	}

	return $ITEMS;
}

function getMinMax($PROP,$FILTER=false){

	$ITEMS = array();
	$arFilter = Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ACTIVE"=>"Y", "!".$PROP => false);
	if($FILTER){
		foreach ($FILTER as $key => $item){
			if(strripos($key,">=") or strripos($key,"<=")){
				unset($FILTER[$key]);
			}
		}
		unset($FILTER["PROPERTY_".$PROP."_VALUE"]);
		$arFilter = array_merge($arFilter,$FILTER);
    }
	$db_list = CIBlockElement::GetList(Array($PROP=>"ASC"), $arFilter, false, array("nTopCount"=>1), array($PROP));
	if($ob = $db_list->Fetch())
	{	
		if($PROP == "PROPERTY_PRICE"){
			 $ob[$PROP."_VALUE"] =  ceil($ob[$PROP."_VALUE"]/1000000);
		}
		$ITEMS["MIN"] = $ob[$PROP."_VALUE"];
	}
	$db_list = CIBlockElement::GetList(Array($PROP=>"DESC"), $arFilter, false, array("nTopCount"=>1), array($PROP));
	if($ob = $db_list->Fetch())
	{	
		if($PROP == "PROPERTY_PRICE"){
			 $ob[$PROP."_VALUE"] =  round($ob[$PROP."_VALUE"]/1000000);
		}
		$ITEMS["MAX"] = $ob[$PROP."_VALUE"];
	}

	return $ITEMS;
}

if($_REQUEST["ACTION"] == "GIVEMEPLAN" && $_REQUEST["ID"]){
$APPLICATION->RestartBuffer(); 
	echo giveMePlan($arFilter,$_REQUEST["ID"]);
die();
} 

// $GLOBALS["filter"][">PROPERTY_PRICE"] = 0;

if($_REQUEST["ACTION"]){
	foreach ($_REQUEST as $key => $item) {
		if(strripos($key,"PROPERTY") !== false){
			if(strripos($item,";") !== false){
				$val = explode(";",$item);
				$GLOBALS["filter"][">=".$key] = $val[0];
				$GLOBALS["filter"]["<=".$key] = $val[1];
			}else{
				$GLOBALS["filter"][$key."_VALUE"] = $item;
			}
		}
	}

	$GLOBALS["filter"]["PROPERTY_STATUS_VALUE"] = "Свободна";
	
	if($GLOBALS["filter"][">=PROPERTY_PRICE"]){
		$GLOBALS["filter"][">PROPERTY_PRICE"] = ceil($GLOBALS["filter"][">=PROPERTY_PRICE"]*1000000);
	}else{
		// $GLOBALS["filter"][">PROPERTY_PRICE"] = 0;
	}

	if($GLOBALS["filter"]["<=PROPERTY_PRICE"]){
		$GLOBALS["filter"]["<=PROPERTY_PRICE"] = round($GLOBALS["filter"]["<=PROPERTY_PRICE"]*1000000);
	}
}

if($_REQUEST["ACTION"] == "AJAX_FILTER"){
	$APPLICATION->RestartBuffer();

	foreach ($_REQUEST as $key => $item) {
		if(strripos($key,"PROPERTY") !== false){
			if(strripos($item,";") !== false){
				$val = explode(";",$item);
				$GLOBALS["filter"][">=".$key] = $val[0];
				$GLOBALS["filter"]["<=".$key] = $val[1];
			}else{
				$GLOBALS["filter"][$key."_VALUE"] = $item;
			}
		}
	}

	$GLOBALS["filter"]["PROPERTY_STATUS_VALUE"] = "Свободна";
	
	if($GLOBALS["filter"][">=PROPERTY_PRICE"]){
		$GLOBALS["filter"][">PROPERTY_PRICE"] = ceil($GLOBALS["filter"][">=PROPERTY_PRICE"]*1000000);
	}

	if($GLOBALS["filter"]["<=PROPERTY_PRICE"]){
		$GLOBALS["filter"]["<=PROPERTY_PRICE"] = round($GLOBALS["filter"]["<=PROPERTY_PRICE"]*1000000);
	}

	$arResult["arFilter"] = $GLOBALS["filter"];

	$arResult["TOWER"] = getUniqueValues("PROPERTY_TOWER",$GLOBALS["filter"]);
	$arResult["ROOMS"] = getUniqueValues("PROPERTY_ROOMS",$GLOBALS["filter"]);
	$arResult["OSOBENNOST"] = getUniqueValues("PROPERTY_OSOBENNOST",$GLOBALS["filter"]);
	$arResult["VID"] = getUniqueValues("PROPERTY_VID",$GLOBALS["filter"]);
	$arResult["FLOOR"] = getMinMax("PROPERTY_FLOOR",$GLOBALS["filter"]);
	$arResult["PRICE"] = getMinMax("PROPERTY_PRICE",$GLOBALS["filter"]);
	$arResult["SQUARE"] = getMinMax("PROPERTY_SQUARE",$GLOBALS["filter"]);

	echo json_encode($arResult);
	die();
}

if($this->StartResultCache(false, array(
	$arFilter,
	$arOrder,
	$arSelect
))
){
	
	$arResult["TOWER"] = getUniqueValues("PROPERTY_TOWER");
	$arResult["ROOMS"] = getUniqueValues("PROPERTY_ROOMS");
	$arResult["OSOBENNOST"] = getUniqueValues("PROPERTY_OSOBENNOST");
	$arResult["VID"] = getUniqueValues("PROPERTY_VID");
	$arResult["FLOOR"] = getMinMax("PROPERTY_FLOOR");
	$arResult["PRICE"] = getMinMax("PROPERTY_PRICE");
	$arResult["SQUARE"] = getMinMax("PROPERTY_SQUARE");

	$this->SetResultCacheKeys(array(
		"TOWER",
		"ROOMS",
		"FLOOR",
		"PRICE",
		"SQUARE",
	));

	$this->IncludeComponentTemplate();
}

?>