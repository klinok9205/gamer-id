$(document).ready(function () {

  $('.burger').click(function () {
    if ($('.burger,.main-menu,body').hasClass('open')) {
      $('.burger,.main-menu,body').removeClass('open');
    } else {
      $('.burger,.main-menu,body').addClass('open');
    }
  });

  function move1Tomove2() {
    if (!document.querySelector(".move-1 > .move-2")) {
      var move2Block = document.querySelector(".move-2");
      var move1Block = document.querySelector(".move-1");
      if (move2Block && move1Block) {
        move2Block.appendChild(move1Block);
      }
    }
  }

  function handleResize() {
    var width = window.innerWidth;

    if (width < 1024) {
      move1Tomove2();
      return;
    }

  }

  window.addEventListener("resize", handleResize);
  handleResize();


  var clicked = 0;

  $(".toggle-password").click(function (e) {
    e.preventDefault();

    $(this).toggleClass("toggle-password");
    if (clicked == 0) {
      $(this).html('<span class="material-icons"><svg class="icon"><use xlink:href="#eye-icon"></use></svg></span>');
      clicked = 1;
    } else {
      $(this).html('<span class="material-icons"><svg class="icon"><use xlink:href="#eye-icon"></use></svg></span>');
      clicked = 0;
    }

    var input = $($(this).attr("toggle"));
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });


});


$(".logined__avatar").click(function () {
  $('.dropdown-menu').toggle();
});
$(document).on('click', function (e) {
  if (!$(e.target).closest(".logined").length) {
    $('.dropdown-menu').hide();
  }
  e.stopPropagation();
});

if (document.documentElement.clientWidth > 992) {

    $(window).on("load", function () {
      $(".scroll").mCustomScrollbar();
    });
 
}