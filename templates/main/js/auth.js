$('form').on('submit',function(){
	var form_data = $(this).serialize();
	var form = $(this);
	var btn = $(this).find('button')

	$.ajax({
	  url: "",
	  method: "POST",
	  data: form_data,
	  dataType: "json",
	  success: function(response){
	  	console.log(response);
	  	if(response.error_html){
	  		$('.errors').remove();
	  		btn.before('<div class="errors"><div style="color:red;background: white;padding: 10px;">'+response.error_html+'</div><br></div>');

	  		if(response.keys){

	  			var errors = response.keys;

	  			for (var i = 0; i < errors.length; i++) {
	  				// console.log(errors[i]);
	  				$('[name="'+errors[i]+'"]').addClass('error');
	  			}
	  				
	  		}

	  		// console.log($)

	  		//name="login"
	  	}else{
	  		if(response.action == 'auth' || response.action == 'register'){
	  			// успешная авторизация или регистрация
		  		if(response.result == true){
		  			// перенаправление в пофиль
		  			location.reload();
		  		}
		  	}
	  		if(response.action == 'forgot_password'){
	  			// успешная авторизация или регистрация
		  		if(response.result == true){
		  			// перенаправление в пофиль
		  			form.html('<label for="" class="active">'+
		  				'<p>На Ваш E-mail отправлено письмо с ссылкой на восстановление пароля!</p><br>'+
		  				'<p>Для смены пароля пожалуйста перейдите по ссылке в письме.</p></label><br>'+
		  				'<p>Если письмо по какой либо причине не пришло, обратитесь к администрации сайта.</p></label><br>'+
		  				'<div class="form-page__bottom">'+
			            '<a href="/" class="back"><span class="icon"><img src="/local/templates/main/images/back-arrow.svg" alt=""></span>назад к логину</a>'+
			            '</div>');
		  			// location.reload();
		  		}
		  	}

	  	}
	  }
	});
});
$('body').on('click','form input',function(){
	$('form .error').removeClass('error');
	$('.errors').remove();
});

$('body').on('click','.oauth',function(){
	if($(this).attr('data-type')){
		var type = $(this).attr('data-type');
		switch (type) {
		  case "discord":
		    console.log("discord oauth.");
		    window.open('/local/ajax/discord/?action=login',type,'top=15, left=20, width=500, height=500');
		    // https://discordapp.com/api/oauth2/applications/960146758732767273
		 //    $.ajax({
			// 	url: 'https://discordapp.com/api/oauth2/applications/960146758732767273',         /* Куда пойдет запрос */
			// 	method: 'POST',
			// 	data: {
			// 		  "id": "960146758732767273",
			// 		  "name": "OAuth2 Test",
			// 		  "description": "",
			// 		  "icon": null,
			// 		  "secret": "dk3aLk2Og6Q_QdtrQ-6AWjB7wqxnLSoJ",
			// 		  "redirect_uris": ["http://two.test"],
			// 		},     /* Параметры передаваемые в запросе. */
			// 	success: function(data){   /* функция которая будет выполнена после успешного запроса.  */
			// 		console.log(data);            /* В переменной data содержится ответ от index.php. */
			// 	}
			// });
		    break;
		  case "steam":
		    console.log("steam oauth.");

		    $.ajax({
				url: 'https://api.steampowered.com/ISteamUserOAuth/GetTokenDetails/v1/?access_token=F77FCC3E497441155970B30EC2EBF253',         /* Куда пойдет запрос */
				method: 'GET',     /* Параметры передаваемые в запросе. */
				success: function(data){   /* функция которая будет выполнена после успешного запроса.  */
					console.log(data);            /* В переменной data содержится ответ от index.php. */
				}
			});


		    // window.open('https://steamcommunity.com/oauth/login?response_type=&client_id=F77FCC3E497441155970B30EC2EBF253&state=',type,'top=15, left=20, width=500, height=500');
		    break;
		  default:
		    console.log("Sorry, we are out of " + type + ".");
		}
	}

})