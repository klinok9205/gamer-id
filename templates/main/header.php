<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!doctype html>
<html>
<head>
<?$APPLICATION->ShowHead();?>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, user-scalable=no">
<title><?$APPLICATION->ShowTitle()?></title>
<?$APPLICATION->ShowPanel()?>
<link href="<?php echo SITE_TEMPLATE_PATH ?>/css/styles.css?<?=time();?>" rel="stylesheet" type="text/css">
</head>
<?
global $USER;
if (!$USER->IsAuthorized()){
require(__DIR__ . "/auth.php");
die();
}?>
<body>
  <header>
    <div class="container">
      <div class="header-content">
        <span class="logo-block">
          <a href="#" class="logo"><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/logo.png" alt=""></a>
        </span>

        <nav class="main-menu">
          <div class="main-menu__content move-2">
            <ul>
              <li class="active"><a href="">Главная</a></li>
              <li><a href="">Продукты</a></li>
              <li><a href="">Поддержка</a></li>
            </ul>
          </div>
          
        </nav>

        <div class="header-content__right">
          <div class="notifications">
            <a href="#">
              <svg class="icon">
                <use xlink:href="#notifications-icon"></use>
              </svg>
              <span>12</span>
            </a>
          </div>

          <div class="logined">
            <span class="logined__avatar"><img src="<?php echo SITE_TEMPLATE_PATH ?>/images/user-empty.png" alt=""></span>

            <div class="dropdown-menu">
              <div class="dropdown-menu__content">
                <span class="dropdown-menu__name">
                  Станислав
                  Верхнегорский
                </span>
                <span class="dropdown-menu__id">
                  ID: <span>17890</span>
                </span>
                <span class="dropdown-menu__exit">
                  <a href="" class="btn btn--full btn--lg">выход</a>
                </span>
              </div>

            </div>
          </div>

          <a class="burger"><span></span></a>
        </div>
      </div>

    </div>
  </header>

  <main>